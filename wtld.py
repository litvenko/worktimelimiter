#!/usr/bin/python3

import os
import sys
import argparse
import configparser
import datetime
import time

configFileName = os.path.expanduser('~/.config/wtldrc')


def logout():
    os.system('qdbus org.kde.ksmserver /KSMServer logout 0 0 0')


def main(arguments):
    # Parse arguments
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--shutdown', action='store_true', help='daemon shutdown')

    args = parser.parse_args(arguments)

    # Parse configuration
    config = configparser.ConfigParser()

    if os.path.isfile(configFileName):
        config.read(configFileName)

    if not 'General' in config:
        config['General'] = {}

    # Check shutdown
    if args.shutdown:
        lastLogoutTime = datetime.datetime.now()

        lastLoginTime = datetime.datetime.strptime(config['General']['LastLoginTime'], '%Y-%m-%d %H:%M:%S.%f') \
            if 'LastLoginTime' in config['General'] else lastLogoutTime

        elapsedDailyTime = float(config['General']['ElapsedDailyTime']) \
            if 'ElapsedDailyTime' in config['General'] else 0

        elapsedDailyTime += (lastLogoutTime - lastLoginTime).total_seconds() / 60

        config['General']['ElapsedDailyTime'] = str(elapsedDailyTime)

        with open(configFileName, 'w+') as configFile:
            config.write(configFile)

        return

    # General
    lastLoginTime = datetime.datetime.now()

    prevLoginTime = datetime.datetime.strptime(config['General']['LastLoginTime'], '%Y-%m-%d %H:%M:%S.%f') \
        if 'LastLoginTime' in config['General'] else lastLoginTime

    config['General']['LastLoginTime'] = str(lastLoginTime)

    if prevLoginTime.date() < lastLoginTime.date():
        config['General']['ElapsedDailyTime'] = '0'

    with open(configFileName, 'w+') as configFile:
        config.write(configFile)

    allowedDailyTime = float(config['General']['AllowedDailyTime']) \
        if 'AllowedDailyTime' in config['General'] else 0

    elapsedDailyTime = float(config['General']['ElapsedDailyTime']) \
        if 'ElapsedDailyTime' in config['General'] else 0

    while True:
        time.sleep(15)

        currentTime = datetime.datetime.now()

        elapsedTime = elapsedDailyTime + (currentTime - lastLoginTime).total_seconds() / 60

        if elapsedTime >= allowedDailyTime:
            logout()
            return


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
